#include "Header.h"

bool checkData (string input)
{
	ifstream infile (input, ifstream::binary);	
	if (infile) 
	{
		infile.close ();
		return true;
	}

	else
		return false;
}

bool checkPass (string p, string password)
{
	int check = 0;
	ifstream infile (p, ifstream::binary);	
	string line, data;
	if (infile) 
	{
		while (getline(infile , line)) 
		{
			if (check == 1)
			{
				istringstream iss(line);
				iss >> data;
			}
			if (check == 2)
				break;
			check++;

		}
		infile.close();
		
	}

	if (password == data)
		return true;
	else 
		return false;
}

detail Max (player a)
{
	detail temp;
	for (int i = 0; i < a.data.size(); i++)
	{
		for (int j = i +1; j < a.data.size(); j++)
			if (a.data[i].score < a.data[j].score)
			{
				temp = a.data[i];
				a.data[i] = a.data[j];
				a.data[j] = temp;
			}
	}

	return a.data[0];
}

void ReRank (vector <Rank> &a)
{
	Rank temp;
	for (int i = 0; i < a.size(); i++)
	{
		for (int j = i +1; j < a.size(); j++)
			if (a[i].data.score < a[j].data.score)
			{
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
	}
}

void updateData (string p, string password, int score)
{
	detail d, sample;

	time_t now = time(0);
	tm *ltm = localtime(&now);

	d.score = score;
	d.day = ltm->tm_mday;
	d.month = 1 + ltm->tm_mon;
	d.year = 1900 + ltm->tm_year;
	d.hour = ltm->tm_hour;
	d.min = ltm->tm_min;
	d.second = ltm->tm_sec;

	player a;
	a.name = p;
	a.password = password;
	string format1, format2;
	format1 = ".process.bin";
	format2 = ".data.bin";

	readFile(a, p+format1);

	a.data.push_back(d);

	sample = Max(a);

	a.Max = sample.score;

	//multiprocess
	thread process_1 (writeFile, a, p, format1, format2);
	thread process_2 (updateRank, p, sample);

	process_1.join();
	process_2.join();
}

void updateRank (string p, detail sample)
{
	vector <Rank> a;
	Rank d;
	string format1, format2;
	format1 = "rank.process.bin";
	format2 = "rank.data.bin";
	
	d.name = p;
	d.data.day = sample.day;
	d.data.month = sample.month;
	d.data.year = sample.year;
	d.data.hour = sample.hour;
	d.data.min = sample.min;
	d.data.second = sample.second;
	d.data.score = sample.score;


	readRank (a, format1);
	int count = 0;
	for (int i = 0; i < a.size(); i++)
		if (a[i].name == d.name)
		{
			count++;
			a[i] = d;
		}

	a.push_back(d);
	if (count!=0)
		a.pop_back();

	
	ReRank (a);

	writeRank(a, format1, format2);
}
