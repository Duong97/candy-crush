#include "Header.h"

void setFontSize(int FontSize)
{
	CONSOLE_FONT_INFOEX info = {0};
	info.cbSize = sizeof(info);
	info.dwFontSize.Y = FontSize;
	info.FontWeight = FW_NORMAL;
	wcscpy(info.FaceName, L"Calibri Console");
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), NULL, &info);
}

void TextColor(int x)
{
	HANDLE color;
	color = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(color,x);
}

void Welcome ()
{
	cout << endl << endl << endl << endl << endl;
	int s = 48;
	cout << setw(s) << " " << "<Candy Crush - introduction>\n";
	cout << setw(s) << " " << "1. Start game\n";
	cout << setw(s) << " " << "2. Information\n";
	cout << setw(s) << " " <<"3. User Management\n";
	cout << setw(s) << " " <<"4. Quit\n";
	cout << setw(s) << " " << "Please pick your choice: ";
}

char Print (char a)
{
	if (a == '1')
		TextColor(9);

	else if (a == '2')
		TextColor(10);

	else if (a == '3')
		TextColor(12);
		
	else if (a == '4')
		TextColor(13);
	
	else if (a == '5')
		TextColor(14);

	else if (a == 'A')
		TextColor(11);

	else if (a == 'B')
		TextColor(4);

	else if (a == 'C')
		TextColor (7);

	return a;
};

void Implement ()
{
	initwindow(1280, 720);
	stable_letter ();
	dynamic_letter ();
	getch();
	closegraph();   
}

void stable_letter ()
{
	//C
	setcolor(8);
	arc (150, 100, 0, 180, 50);
	arc (150, 150, 180, 360, 50);
	line (100, 100, 100, 150);

	//A
	setcolor(10);
	line (250, 50, 260, 50);
	line (250, 50, 205, 200);
	line (260, 50, 305, 200);
	line (220, 150, 290, 150);

	//N
	setcolor(3);
	line (310+5, 50, 310+5, 200);
	line (410-5, 50, 410-5, 200);
	line (310+5, 50, 410-5, 200);

	//D
	setcolor(12);
	line (415, 50, 435, 50);
	line (415, 50, 415, 200);
	line (415, 200, 435, 200);
	arc (435, 125, 270, 90, 75);

	//Y
	setcolor(5);
	line (515, 50, 565, 125);
	line (615, 50, 565, 125);
	line (565, 125, 565, 200);

	//C
	setcolor(9);
	arc (715, 100, 0, 180, 50);
	line (665, 100, 665, 150);
	arc (715, 150, 180, 360, 50);

	//R
	setcolor (2);
	line (770+5, 50, 770+5, 200);
	line (770+5, 50, 832.5, 50);
	line (770+5, 125, 832.5, 125);
	arc (870 - 37.5, 88, 270, 90, 37.5);
	line (832.5, 125, 870, 200);

	//U
	setcolor (11);
	line (875+5, 50, 875+5, 180);
	line (970 -5, 50, 970-5, 180);
	line (895+5, 200, 950 -5, 200);
	arc (895+5, 180, 180, 270, 20);
	arc (950-4, 180, 270, 360, 20);

	//S
	setcolor (4);
	arc (1025, 100, 0, 180, 50);
	arc (1025, 150, 180, 360, 50);
	arc (1025, 62.5+0.5, 180 + (90 - 53), 270, 62.5);
	arc (1026, 187.5-0.5, 90 - 52.1, 90, 62.5);

	//H
	setcolor (14);
	line (1080+5, 50, 1080+5, 200);
	line (1180 -5, 200, 1180-5, 50);
	line (1080 +5, 125, 1180-5, 125);

	//Circle
	setfillstyle(1,4);
	setcolor (4);
	circle (540, 300, 45);
	floodfill (540, 300, 4);

	circle (640, 300, 45);
	floodfill (640, 300, 4);

	setfillstyle(1,5);
	setcolor (5);
	circle (540, 400, 45);
	floodfill (540, 400, 5);

	circle (640, 500, 45);
	floodfill (640, 500, 5);

	setfillstyle(1,6);
	setcolor (6);
	circle (540, 500, 45);
	floodfill (540, 500, 6);
}

void dynamic_letter ()
{
	while (!kbhit())
	{
		//1st stage
		setfillstyle (1, 1);
		setcolor (1);
		circle (740, 300, 45);
		floodfill (740, 300, 1);

		circle (640, 400, 45);
		floodfill (640, 400, 1);

		circle (740, 500, 45);
		floodfill (740, 500, 1);

		setfillstyle (1, 2);
		setcolor (2);
		circle (740, 400, 45);
		floodfill (740, 400, 2);
		delay (200);

		//ENTER
		for (int i = 9; i <= 11; i++)
		{
			setcolor (i);
			line (255+315, 430+195, 275+315, 430+195);
			line (255+315, 430+195, 255+315, 450+195);
			line (255+315, 450+195, 275+315, 450+195);
			line (255+315, 450+195, 275+315, 450+195);
			line (255+315, 440+195, 275+315, 440+195);
			line (285+315, 430+195, 285+315, 450+195);
			line (285+315, 430+195, 305+315, 450+195);
			line (305+315, 430+195, 305+315, 450+195);
			line (315+315, 430+195, 335+315, 430+195);
			line (325+315, 430+195, 325+315, 450+195);
			line (345+315, 430+195, 365+315, 430+195);
			line (345+315, 440+195, 365+315, 440+195);
			line (345+315, 450+195, 365+315, 450+195);
			line (345+315, 430+195, 345+315, 450+195);
			arc (390+315, 435+195, 270, 90, 5);
			line (375+315, 430+195, 390+315, 430+195);
			line (375+315, 440+195, 390+315, 440+195);
			line (375+315, 430+195, 375+315, 450+195);
			line (390+315, 440+195, 395+315, 450+195);
			delay (200);
		}
		//2nd stage
		setfillstyle (1, 0);
		setcolor (0);
		circle (640, 400, 45);
		floodfill (640, 400, 0);

		setfillstyle (1, 2);
		setcolor (2);
		circle (640, 400, 45);
		floodfill (640, 400, 2);
		
		setfillstyle (1, 0);
		setcolor (0);
		circle (740, 400, 45);
		floodfill (740, 400, 0);
		
		setfillstyle (1, 1);
		setcolor (1);
		circle (740, 400, 45);
		floodfill (740, 400, 1);
		delay (200);

		//3rd stage
		setfillstyle (1, 0);
		setcolor (0);
		circle (740, 300, 45);
		floodfill (740, 300, 0);
		delay (200);

		circle (740, 400, 45);
		floodfill (740, 400, 0);
		delay (200);

		circle (740, 500, 45);
		floodfill (740, 500, 0);
		delay (200);
	}
}