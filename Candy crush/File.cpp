#include "Header.h"

void writeFileNumber(string fileName, int fValue){
	ofstream ofs (fileName, ofstream::out| ofstream::app | ofstream::binary);
	ofs << fValue;
	ofs.close();
}

void writeFileString(string fileName, string strValue){
	ofstream ofs (fileName, ofstream::out| ofstream::app | ofstream::binary);
	ofs << strValue;
	ofs.close();
}

void writeFileNewLine(string fileName){
	ofstream ofs (fileName, ofstream::out| ofstream::app | ofstream::binary);
	ofs << endl;
	ofs.close();
}

void clearFile(string fileName){
	ofstream ofs (fileName, ofstream::out | ofstream::binary);
	ofs.close();
}

void readFile (player &a, string input)
{
	int check = 0;
	detail temp;
	ifstream infile(input, ifstream::binary);	
	string line;

	if (infile) 
	{
		while (getline(infile , line)) 
		{
			if (check < 3)
				check++;
			else 
			{
				istringstream iss(line);
				iss >> temp.day;
					
				iss >> temp.month;
				iss >> temp.year;
				iss >> temp.hour;
				iss >> temp.min;
				iss >> temp.second;
				iss >> temp.score;
				
				a.data.push_back(temp);
			}
		}
		infile.close();
		
	}

};

void readRank (vector <Rank> &a, string input)
{
	int check = 0, count = 0;

	ifstream infile(input, ifstream::binary);	
	string line;

	if (infile) 
	{
		while (getline(infile , line)) 
		{
			if (check < 2)
				check++;
			else 
			{
				istringstream iss(line);
				a.push_back(Rank());
				iss >> a[count].data.day;
				iss >> a[count].data.month;
				iss >> a[count].data.year;
				iss >> a[count].data.hour;
				iss >> a[count].data.min;
				iss >> a[count].data.second;
				iss >> a[count].name;
				iss >> a[count].data.score;
				count++;
			}
		}
		infile.close();
	}
}

void writeFile (player a, string p, string format1, string format2)
{
	thread process_1 (writeFile_process, a, p, format1);
	thread process_2 (writeFile_data, a, p, format2);

	process_1.join();
	process_2.join();
}

void writeFile_process (player a, string p, string format1)
{
	clearFile(p+format1);
	writeFileString(p+format1, a.name);
	writeFileNewLine(p+format1);
	writeFileString(p+format1, a.password);
	writeFileNewLine(p+format1);
	writeFileNumber(p+format1, a.Max);
	writeFileNewLine(p+format1);

	for (int i = 0; i < a.data.size(); i++)
	{
		writeFileNumber(p+format1, a.data[i].day);
		writeFileString(p+format1, " ");
		writeFileNumber(p+format1, a.data[i].month);
		writeFileString(p+format1, " ");
		writeFileNumber(p+format1, a.data[i].year);
		writeFileString(p+format1, "		");
		writeFileNumber(p+format1, a.data[i].hour);
		writeFileString(p+format1, " ");
		writeFileNumber(p+format1, a.data[i].min);
		writeFileString(p+format1, " ");
		writeFileNumber(p+format1, a.data[i].second);
		writeFileString(p+format1, "		");
		writeFileNumber(p+format1, a.data[i].score);
		writeFileNewLine(p+format1);
	}
}

void writeFile_data (player a, string p, string format2)
{
	clearFile(p+format2);
	writeFileString(p+format2, a.name);
	writeFileNewLine(p+format2);
	writeFileString(p+format2, "The best achievement: ");
	writeFileNumber(p+format2, a.Max);
	writeFileNewLine(p+format2);
	writeFileNewLine(p+format2);
	
	writeFileString(p+format2, "Date      ");
	writeFileString(p+format2, "		");
	writeFileString(p+format2, "Local Time");
	writeFileString(p+format2, "		");
	writeFileString(p+format2, "Score");

	writeFileNewLine(p+format2);
	for (int i = 0; i < a.data.size(); i++)
	{
		writeFileNumber(p+format2, a.data[i].day);
		writeFileString(p+format2, "/");
		writeFileNumber(p+format2, a.data[i].month);
		writeFileString(p+format2, "/");
		writeFileNumber(p+format2, a.data[i].year);
		writeFileString(p+format2, "		");
		writeFileNumber(p+format2, a.data[i].hour);
		writeFileString(p+format2, ":");
		writeFileNumber(p+format2, a.data[i].min);
		writeFileString(p+format2, ":");

		writeFileNumber(p+format2, a.data[i].second);
		if (a.data[i].hour < 10 || a.data[i].min < 10 || a.data[i].second < 10)
			writeFileString(p+format2, "	");
		writeFileString(p+format2, "		");

		writeFileNumber(p+format2, a.data[i].score);
		writeFileNewLine(p+format2);
	}
}

void writeRank (vector <Rank> a, string format1, string format2)
{
	thread process_1 (writeRank_process, a, format1);
	thread process_2 (writeRank_data, a, format2);

	process_1.join();
	process_2.join();
}

void writeRank_process (vector <Rank> a, string format1)
{
	clearFile(format1);
	writeFileString(format1, "Rank");
	writeFileNewLine(format1);
	writeFileNewLine(format1);

	for (int i = 0; i < a.size(); i++)
	{
		writeFileNumber(format1, a[i].data.day);
		writeFileString(format1, " ");
		writeFileNumber(format1, a[i].data.month);
		writeFileString(format1, " ");
		writeFileNumber(format1, a[i].data.year);
		writeFileString(format1, "		");
		writeFileNumber(format1, a[i].data.hour);
		writeFileString(format1, " ");
		writeFileNumber(format1, a[i].data.min);
		writeFileString(format1, " ");
		writeFileNumber(format1, a[i].data.second);
		writeFileString(format1, "		");
		writeFileString(format1, a[i].name);
		writeFileString(format1, "		");
		writeFileNumber(format1, a[i].data.score);
		writeFileNewLine(format1);
	}
}

void writeRank_data (vector <Rank> a, string format2)
{
	clearFile(format2);
	writeFileString(format2, "Rank");
	writeFileNewLine(format2);
	writeFileNewLine(format2);
	
	writeFileString(format2, "Date      ");
	writeFileString(format2, "		");
	writeFileString(format2, "Local Time");
	writeFileString(format2, "		");
	writeFileString(format2, "Name      ");
	writeFileString(format2, "		");
	writeFileString(format2, "Score");
	writeFileNewLine(format2);

	for (int i = 0; i < a.size(); i++)
	{
		writeFileNumber(format2, a[i].data.day);
		writeFileString(format2, "/");
		writeFileNumber(format2, a[i].data.month);
		writeFileString(format2, "/");	
		writeFileNumber(format2, a[i].data.year);
		writeFileString(format2, "		");
		writeFileNumber(format2, a[i].data.hour);
		writeFileString(format2, ":");
		writeFileNumber(format2, a[i].data.min);
		writeFileString(format2, ":");

		writeFileNumber(format2, a[i].data.second);
		if (a[i].data.hour < 10 || a[i].data.min < 10 || a[i].data.second < 10)
			writeFileString(format2, "	");
		writeFileString(format2, "		");
		
		writeFileString(format2, a[i].name);
		for (int j = 0; j < 10 - a[i].name.length(); j++)
			writeFileString(format2, " ");

		writeFileString(format2, "		");
		writeFileNumber(format2, a[i].data.score);
		writeFileNewLine(format2);
	}
}

bool showData (string input)
{
	ifstream infile(input, ifstream::binary);	
	string line;

	if (infile) 
	{
		while (getline(infile , line)) 
		{
			cout << line << endl;
		}
		infile.close();
		return true;
	}

	else
		return false;
}
