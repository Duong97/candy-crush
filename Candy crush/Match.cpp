#include "Header.h"

bool combo (char a[9][9], direction d, string select, int &score)
{
	//Stripe and stripe
	if (a[select[0]][select[1]] == 'A' && a[d.C_r][d.C_l] == 'A')
	{
		score+= 17;
		for (char j = '0'; j < d.C_l; j++)
			a[d.C_r][j] = ' ';
		
		for (char i = '0'; i < '9'; i++)
			 a[i][d.C_l] = ' ';
		
		for (char j = d.C_l; j < '9'; j++)
			a[d.C_r][j] = ' ';
		
		return true;
	}

	//Stripe and exploding
	if ((a[select[0]][select[1]] == 'A' && a[d.C_r][d.C_l] == 'C') || (a[select[0]][select[1]] == 'C' && a[d.C_r][d.C_l] == 'A'))
	{
		score+= 45;
		//1st
		for (char i = '0'; i < d.up1; i++)
		{
			a[i][d.left1] = ' ';
			a[i][d.C_l] = ' ';
			a[i][d.right1] = ' ';
		}

		for (char j = '0'; j < '9'; j++)
			a[d.up1][j] = ' ';
		
		//2nd
		for (char j = '0'; j < '9'; j++)
			a[d.C_r][j] = ' ';
		
		//3rd
		for (char j = '0'; j < '9'; j++)
			a[d.down1][j] = ' ';

		for (char i = d.down1 + 1; i < '9'; i++)
		{
			a[i][d.left1] = ' ';
			a[i][d.C_l] = ' ';
			a[i][d.right1] = ' ';
		}
		return true;
	}

	//Exploding and exploding
	if (a[select[0]][select[1]] == 'C' && a[d.C_r][d.C_l] == 'C')
	{
		score+=25;
		for (char i = d.up2; i <= d.down2; i++)
			for (char j = d.left2; j <= d.right2; j++)
				a[i][j] = ' ';
			
		return true;
	}

	//Star and normal number or specific charater except star
	if ((a[select[0]][select[1]] == 'B' && a[d.C_r][d.C_l] != 'B') || (a[select[0]][select[1]] != 'B' && a[d.C_r][d.C_l] == 'B'))
	{
		char sample;
		score+=2;
		if (a[d.C_r][d.C_l] == 'B')
		{
			sample = a[select[0]][select[1]];
			for (char i = '0'; i < '9'; i++)
			{
				for (char j = '0'; j < '9'; j++)
				{
					if (a[i][j] == sample)
					{
						if (sample == 'A' || sample == 'C')
						{
							score+=2;
							a[i][j] = ' ';
							
						}
						
						else 
						{
							score+=1;
							a[i][j] = ' ';
					
						}
					}
				}
			}
			
			a[d.C_r][d.C_l]= ' ';
		
		}

		else 
		{
			sample = a[d.C_r][d.C_l];
			for (char i = '0'; i < '9'; i++)
			{
				for (char j = '0'; j < '9'; j++)
				{
					if (a[i][j] == sample)
					{
						if (sample == 'A' || sample == 'C')
						{
							score+=2;
							a[i][j] = ' ';
						
						}
						
						else 
						{
							score+=1;
							a[i][j] = ' ';
						
						}
					}
				}
			}

			a[select[0]][select[1]] = ' ';
		
		}
	}

	//Star and star
	if (a[select[0]][select[1]] == 'B' && a[d.C_r][d.C_l] == 'B')
	{
		score+=81;
		Initial_table(a);
	}
	return false;
};

bool match_3 (char a[9][9], direction d, int &score)
{
	//horizontal
	//1st position
	if (a[d.C_r][d.C_l] == a[d.C_r][d.right1] && a[d.C_r][d.C_l] == a[d.C_r][d.right2])
	{
		score+=3;
		a[d.C_r][d.C_l] = ' ';
		a[d.C_r][d.right1] = ' ';
		a[d.C_r][d.right2] = ' ';
	
		return true;
	}

	//2nd postion
	else if (a[d.C_r][d.C_l] == a[d.C_r][d.right1] && a[d.C_r][d.C_l] == a[d.C_r][d.left1])
	{
		score+=3;
		a[d.C_r][d.left1] = ' ';
		a[d.C_r][d.C_l] = ' ';
		a[d.C_r][d.right1] = ' ';
	
		return true;
	}

	//3rd position
	else if (a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.C_r][d.left2])
	{
		score+=3;
		a[d.C_r][d.left2] = ' ';
		a[d.C_r][d.left1] = ' ';
		a[d.C_r][d.C_l] = ' ';
		
		return true;
	}

	//vertical
	//1st postion
	else if (a[d.C_r][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.C_l] == a[d.down2][d.C_l])
	{
		score+=3;
		a[d.C_r][d.C_l] = ' ';
		a[d.down1][d.C_l] = ' ';
		a[d.down2][d.C_l] = ' ';
		return true;
	}

	//2nd position
	else if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.down1][d.C_l])
	{
		score+=3;
		a[d.up1][d.C_l] = ' ';
		a[d.C_r][d.C_l] = ' '; 
		a[d.down1][d.C_l] = ' ';
		return true;
	}

	//3rd position
	else if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.up2][d.C_l])
	{
		score+=3;
		a[d.up2][d.C_l] = ' ';
		a[d.up1][d.C_l] = ' ';
		a[d.C_r][d.C_l] = ' '; 
		
		return true;
	}
	
	return false;
};

bool match_4 (char a[9][9], direction d, int &score)
{
	//horizontal
	//2nd position
	if (a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.C_r][d.right1] && a[d.C_r][d.C_l] == a[d.C_r][d.right2])
	{
		score+=4;
		a[d.C_r][d.left1] = ' ';
		
		a[d.C_r][d.C_l] = 'A';
		
		a[d.C_r][d.right1] = ' ';
		a[d.C_r][d.right2] = ' ';
		return true;
	}

	//3rd position
	else if (a[d.C_r][d.C_l] == a[d.C_r][d.left2] && a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.C_r][d.right1])
	{
		score+=4;
		a[d.C_r][d.left2] = ' ';
		a[d.C_r][d.left1] = ' ';
		
		a[d.C_r][d.C_l] = 'A';
		
		a[d.C_r][d.right1] = ' ';
		return true;
	}

	//vertical
	//2nd position
	else if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.C_l] == a[d.down2][d.C_l])
	{
		score+=4;
		a[d.up1][d.C_l] = ' ';
	
		a[d.C_r][d.C_l] = 'A';
		
		a[d.down1][d.C_l] = ' ';
		a[d.down2][d.C_l] = ' ';
		return true;
	}

	//3rd position
	else if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.up2][d.C_l] && a[d.C_r][d.C_l] == a[d.down1][d.C_l])
	{
		score+=4;
		a[d.up2][d.C_l] = ' ';
		a[d.up1][d.C_l] = ' ';
		
		a[d.C_r][d.C_l] = 'A';
		
		a[d.down1][d.C_l] = ' ';
		return true;
	}

	return false;
}

bool match_5 (char a[9][9], direction d, int &score)
{
	//horizontal
	if (a[d.C_r][d.left2] == a[d.C_r][d.left1] && a[d.C_r][d.right1] == a[d.C_r][d.right2])
	{
		if (a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.C_r][d.right1])
		{
			score+=5;
			a[d.C_r][d.left2] = ' ';
			a[d.C_r][d.left1] = ' ';
			//Random (a, d.C_r, d.left2);
			//Random (a, d.C_r, d.left1);
			a[d.C_r][d.C_l] = 'B';
			//Random (a, d.C_r, d.right1);
			//Random (a, d.C_r, d.right2);
			a[d.C_r][d.right1] = ' ';
			a[d.C_r][d.right2] = ' ';
			return true;
		}
	}

	//vertical
	else if (a[d.up2][d.C_l] == a[d.up1][d.C_l] && a[d.down1][d.C_l] == a[d.down2][d.C_l])
	{
		if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.down1][d.C_l])
		{
			score+=5;
			a[d.up2][d.C_l] = ' ';
			a[d.up1][d.C_l] = ' ';
			//Random (a, d.up2, d.C_l);
			//Random (a, d.up1, d.C_l);
			a[d.C_r][d.C_l] = 'B';
			//Random (a, d.down1, d.C_l);
			//Random (a, d.down2, d.C_l);
			a[d.down1][d.C_l] = ' ';
			a[d.down2][d.C_l] = ' ';
			return true;
		}
	}

	return false;
};

bool match_L (char a[9][9], direction d, int &score)
{
	//Angle 3
	if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.up2][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.right1] && a[d.C_r][d.C_l] == a[d.C_r][d.right2])
	{
		if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.right1])
		{
			score+=5;
			a[d.up2][d.C_l] = ' ';
			a[d.up1][d.C_l] = ' ';
			//Random (a, d.up2, d.C_l);
			//Random (a, d.up1, d.C_l);
			a[d.C_r][d.C_l] = 'C';
			//Random (a, d.C_r, d.right1);
			//Random (a, d.C_r, d.right2);
			a[d.C_r][d.right1] = ' ';
			a[d.C_r][d.right2] = ' ';
			return true;
		}
	}

	//Angle 4
	else if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.up2][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.C_r][d.left2])
	{
		if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.left1])
		{
			score+=5;
			a[d.up2][d.C_l] = ' ';
			a[d.up1][d.C_l] = ' ';
			a[d.C_r][d.left2] = ' ';
			a[d.C_r][d.left1] = ' ';
			//Random (a, d.up2, d.C_l);
			//Random (a, d.up1, d.C_l);
			//Random (a, d.C_r, d.left2);
			//Random (a, d.C_r, d.left1);
			a[d.C_r][d.C_l] = 'C';
			return true;
		}
	}

	//Angle 1
	else if (a[d.C_r][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.C_l] == a[d.down2][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.C_r][d.left2])
	{
		if (a[d.C_r][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.left1])
		{
			score+=5;
			a[d.C_r][d.left2] = ' ';
			a[d.C_r][d.left1] = ' ';
			//Random (a, d.C_r, d.left2);
			//Random (a, d.C_r, d.left1);
			a[d.C_r][d.C_l] = 'C';
			//Random (a, d.down1, d.C_l);
			//Random (a, d.down2, d.C_l);
			a[d.down1][d.C_l] = ' ';
			a[d.down2][d.C_l] = ' ';
			return true;
		}
	}

	//Angle 2
	else if (a[d.C_r][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.C_l] == a[d.down2][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.right1] && a[d.C_r][d.C_l] == a[d.C_r][d.right2])
	{
		if (a[d.C_r][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.right1])
		{
			score+=5;
			a[d.C_r][d.C_l] = 'C';
			//Random (a, d.C_r, d.right1);
			//Random (a, d.C_r, d.right2);
			//Random (a, d.down1, d.C_l);
			//Random (a, d.down2, d.C_l);
			a[d.C_r][d.right1] = ' ';
			a[d.C_r][d.right2] = ' ';
			a[d.down1][d.C_l] = ' ';
			a[d.down2][d.C_l] = ' ';
			return true;
		}
	}
	return false;
};

bool match_T (char a[9][9], direction d, int &score)
{
	//north
	if (a[d.C_r][d.left1] == a[d.C_r][d.right1] && a[d.down2][d.C_l] == a[d.down1][d.C_l])
	{
		if (a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.down1][d.C_l])
		{
			score+=5;
			a[d.C_r][d.left1] = ' ';
		
			a[d.C_r][d.C_l] = 'C';
			
			a[d.C_r][d.right1] = ' ';
			a[d.down1][d.C_l] = ' ';
			a[d.down2][d.C_l] = ' ';
			return true;
		}
	}
	
	//east
	else if (a[d.up1][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.left1] == a[d.C_r][d.left2])
	{
		if (a[d.C_r][d.C_l] == a[d.up1][d.C_l] && a[d.C_r][d.C_l] == a[d.C_r][d.left1])
		{
			score+=5;
			a[d.C_r][d.left2] = ' ';
			a[d.C_r][d.left1] = ' ';
			a[d.up1][d.C_l] = ' ';
		
			a[d.C_r][d.C_l] = 'C';
			
			a[d.down1][d.C_l] = ' ';
			return true;
		}
	}

	//south
	else if (a[d.C_r][d.left1] == a[d.C_r][d.right1] && a[d.up1][d.C_l] == a[d.up2][d.C_l])
	{
		if (a[d.C_r][d.C_l] == a[d.C_r][d.left1] && a[d.C_r][d.C_l] == a[d.up1][d.C_l])
		{
			score+=5;
			a[d.up2][d.C_l] = ' ';
			a[d.up1][d.C_l] = ' ';
			a[d.C_r][d.left1] = ' ';
		
			a[d.C_r][d.C_l] = 'C';
		
			a[d.C_r][d.right1] = ' ';
			return true;
		}
	}

	//west
	else if (a[d.up1][d.C_l] == a[d.down1][d.C_l] && a[d.C_r][d.right1] == a[d.C_r][d.right2])
	{
		if (a[d.C_r][d.C_l] == a[d.C_r][d.right1] && a[d.C_r][d.C_l] == a[d.up1][d.C_l])
		{
			score+=5;
			a[d.up1][d.C_l] = ' ';
		
			a[d.C_r][d.C_l] = 'C';
		
			a[d.down1][d.C_l] = ' ';
			a[d.C_r][d.right1] = ' ';
			a[d.C_r][d.right2] = ' ';
			return true;
		}
	}

	return false;
}