#include "Header.h"

void Menu ()
{
	TextColor(7);
	setFontSize(20); //display
	string k;

	system ("cls");

	//Menu screen
	Welcome();
	cin >> k;

	while (k != "1" && k != "2" && k != "3" && k != "4")
	{
		system("cls");
		Welcome ();
		cin >> k;
	};

	if (k == "1")
		execute ();

	else if (k == "2")
		Info();

	else if (k == "3")
		User();

	else if (k == "4")
	{
		system ("cls");
		for (int i = 0; i < 5; i++)
			cout << endl;
		cout << "  Hope to see you soon!\n  " ;
		system ("pause");
	}
}

void execute ()
{
	srand (time(NULL));
	clock_t start, end;

	float Time;
	int initial, score;
	char a[9][9], newR, newC;
	string select, k, p, password, format1, format2;
	format1 = ".process.bin";
	format2 = ".data.bin";

	system ("cls");
	
	for (int i = 0; i < 5; i++)
		cout << endl;


	//Start the program
	while (true) 
	{
		cout << setw (2) << " " << "Username: ";
		cin >> p;

		//Check the length of name (Maximum 10 characters)
		while (p.length() > 10 || p == "con" || p == "coN" || p == "cOn" || p == "cON" || p == "Con" || p == "CoN" || p == "COn" || p == "CON")
		{
			system ("cls");
			for (int i = 0; i < 5; i++)
				cout << endl;
			if (p.length() > 10)
			{
				cout << setw (2) << " " << "You are not allowed to enter a name which exceeds 10 characters, please input again or press '~' to exit: ";
				cin >> p;
			}

			if (p == "con" || p == "coN" || p == "cOn" || p == "cON" || p == "Con" || p == "CoN" || p == "COn" || p == "CON")
			{
				cout << setw (2) << " " << "You are not allowed to enter this name, please input again or press '~' to exit: ";
				cin >> p;
			}

			if (p == "~")
				break;
		}

		if (p == "~")
		{
			//Back to the menu;
			cout << "  ";
			system ("pause");
			Menu ();
			break;
		}
	
		//Check the exsistence of user 
		if (!checkData(p+format1))
		{
			cout <<  setw (2) << " " << "You are the new one, please create the account by entering your new password or press '~' to exit: ";
			cin >> password;
			if (password == "~")
			{
				//Back to the menu;
				cout << "  ";
				system ("pause");
				Menu ();
				break;
			}
		}

		//Check password
		else 
		{
			cout << setw (2) << " " << "Password: ";
			cin >> password;
			while (!checkPass(p+format1, password))
			{
				system ("cls");
				for (int i = 0; i < 5; i++)
					cout << endl;
				cout << setw (2) << " " << "Wrong password, enter again or press '~' to exit: ";
				cin >> password;
				if (password == "~")
					break;
			}

			if (password == "~")
			{
				//Back to the menu;
				cout << "  ";
				system ("pause");
				Menu ();
				break;
			}
		}

		//Create the table
		Time = 500;
		initial = 0;
		Initial_table(a);

		//set up the clock
		start = clock();
	
		//Start to play game
		while (true)
		{
			system("cls");
			//Scan the table
			Scan (a, score);
			for (int i = 0; i < 5; i++)
				cout << endl;
			Show_table(a);
			end = clock();

			//Create initial score
			if (initial == 0)
			{
				score = 0;
				initial = 1;
			}

			TextColor(11);
			cout << endl << setw (29) << " " << "User: " <<  p << endl;
		
			TextColor(14);
			cout << setw (29) << " " << "Score: " << score << endl;

			//Finish the match
			if (((float)(end - start)) / CLOCKS_PER_SEC >= Time)
			{
				updateData (p, password, score);
				TextColor(12);
				cout << setw (29) << " " << "Time's up" << endl;
				TextColor(8);
				cout << setw (29) << " ";
				//Back to the menu;
				system ("pause");
				Menu ();
				break;
			}
	
			else 
			{
				TextColor (12);
				cout << setw (29) << " " << "You still have " << (int)(Time - (end - start) / CLOCKS_PER_SEC)  << " seconds" << endl;
			}

			//Choose the number
			TextColor (15);
			cout << setw (29) << " " << "Choose the number or press '~' to exit: ";
			cin >> select;
			if (select.length() == 1)
			{
				if (select[0] != '~')
				do
				{
					cout << setw (29) << " " << "Choose again: ";
					cin >> select;
					if (select.length() == 2)
						break;
				} while (select[0] != '~');
			}
		
			if (select.length() >= 2)
			{
				while (true)
				{
					if (select.length() == 2 && select[0] <= '8' && select[0] >= '0' && select[1] <= '8' && select[1] >= '0')
						break;
					else
					{
						cout << setw (29) << " " << "Choose again: ";
						cin >> select;
					}
				}
			}
		
			//Exit the match
			if (select[0] == '~')
			{
				updateData (p, password, score);
				TextColor(8);
				cout << setw (29) << " ";
				//Back to the menu;
				system ("pause");
				Menu ();
				break;
			}

			else
			{
				cout << endl << setw (29) << " " << "u is up";
				cout << endl << setw (29) << " " << "d is down";
				cout << endl << setw (29) << " " << "r is right";
				cout << endl << setw (29) << " " << "l is left";
				cout << endl << setw (29) << " " <<	"Choose direction: ";
				cin >> k; 
				while (k!= "u" && k != "d" && k != "r" && k != "l")
				{
					cout << setw (29) << " " << "Choose again: ";
					cin >> k;
				}

				Select (a, k, select, newR, newC);

				//Process after move the numbers
				Find_first (a, newR, newC, select, k, score);
			}
		}

		break;
	}
}

void Info ()
{
	system ("cls");

	TextColor (14);
	cout << endl << endl;
	cout << "  Candy Crush is set of magical world of numbers. This game will bring to you wonderful moments with your friends.\n  You must try it now!\n";
	
	TextColor (8);
	cout << "  ";
	for (int i = 0; i < 122; i++)
		cout << "_";

	cout << endl << endl;

	TextColor (3);
	cout << "  Rule of game\n\n";
	cout << "  You must create an account to enjoy the game. To do this, let choose start a new game option at the beginning. And\n  then enter your new username and new password. Remember that the maximum length of username is 10.\n\n";
	cout << "  Each number will be worth 1 point, please notice that you must choose the number which can be combined with others.\n  Moreover, the time is 500 seconds and you can own some points when the table is being reseted:\n\n";
	cout << "         1. Match 3 numbers to make the empty space, it means that you get 3 points.\n";
	cout << "         2. Match 4 numbers in a row or a column to make a candy A, then you have 4 points.\n";
	cout << "         3. Match 5 numbers in a row or a column to make a candy B, then you have 5 points.\n";
	cout << "         4. Match 5 numbers in a T or L shape to make a candy C, then you have 5 points too.\n";
	cout << "         5. Move candy A to candy A together will swipe a horizontal and vertical, then you get 17 points.\n";
	cout << "         6. Swap candy A for candy C together will erase three horizontal lines and three vertical lines, then you\n            collect 45 points.\n";
	cout << "         7. Shift candy C to candy C together will bring to you 25 points and eliminate a square of 5x5.\n";
	cout << "         8. Transfer from candy B to the others together except candy B will eliminate all the characters which are the\n            same type as the sample. Please notice that each number will be marked by 1 point while special candies like\n            A or C will bring to you 2 points.\n";
	cout << "         9. Combine candy B with candy B will erase the table, it means that you have 81 points!\n";
	
	TextColor (8);
	cout << "  ";
	for (int i = 0; i < 122; i++)
		cout << "_";

	TextColor (12);
	cout << "\n\n  Author:\n";
	cout << "         1. NGUYEN HAI DUONG\n";
	cout << "         2. PHAM MINH DUY\n";
	cout << "         3. TRINH HUU LOC\n\n";
	cout << "  Copyright 2017 Candy Crush Game. All rights reserved.\n\n";

	TextColor (8);
	//Back to the menu;
	cout << "  ";
	system ("pause");
	Menu ();
}

void User ()
{
	string p, password;
	string format1, format2, select;
	format1 = ".process.bin";
	format2 = ".data.bin";

	system ("cls");
	
	cout << endl << endl << endl << endl << endl;
	cout << "  1. Your profile\n  2. Ranking\n  Choose: ";
	cin >> select;


	while (select != "1" && select != "2")
	{
		system ("cls");
		cout << endl << endl << endl << endl << endl;
		cout << "  1. Your profile\n  2. Ranking\n  Choose: ";
		cin >> select;
	};

	//Start
	while (true)
	{
		if (select == "1")
		{
			system ("cls");
			cout << endl << endl << endl << endl << endl;
			cout << "  Username: ";
			cin >> p;

			//Check the length of name (Maximum 10 characters)
			while (p.length() > 10 || p == "con" || p == "coN" || p == "cOn" || p == "cON" || p == "Con" || p == "CoN" || p == "COn" || p == "CON")
			{
				system ("cls");
				for (int i = 0; i < 5; i++)
					cout << endl;
				if (p.length() > 10)
				{
					cout << setw (2) << " " << "You are not allowed to enter a name which exceeds 10 characters, please input again or press '~' to exit: ";
					cin >> p;
				}

				if (p == "con" || p == "coN" || p == "cOn" || p == "cON" || p == "Con" || p == "CoN" || p == "COn" || p == "CON")
				{
					cout << setw (2) << " " << "You are not allowed to enter this name, please input again or press '~' to exit: ";
					cin >> p;
				}

				if (p == "~")
					break;
			}
	
			if (p == "~")
			{
				system ("cls");
				Menu ();
				break;
			}

			//Check the existence of user
			while (!checkData(p+format1))
			{
				system ("cls");
				cout << endl << endl << endl << endl << endl;
				cout << "  Wrong username, input again or press '~' to exit: ";
				cin >> p;

				//Check the length of name (Maximum 10 characters)
				while (p.length() > 10 || p == "con" || p == "coN" || p == "cOn" || p == "cON" || p == "Con" || p == "CoN" || p == "COn" || p == "CON")
				{
					system ("cls");
					for (int i = 0; i < 5; i++)
						cout << endl;
					if (p.length() > 10)
					{
						cout << setw (2) << " " << "You are not allowed to enter a name which exceeds 10 characters, please input again or press '~' to exit: ";
						cin >> p;
					}

					if (p == "con" || p == "coN" || p == "cOn" || p == "cON" || p == "Con" || p == "CoN" || p == "COn" || p == "CON")
					{
						cout << setw (2) << " " << "You are not allowed to enter this name, please input again or press '~' to exit: ";
						cin >> p;
					}

					if (p == "~")
						break;
				}

				if (p == "~")
					break;
			}

			if (p == "~")
			{
				system ("cls");
				Menu ();
				break;
			}
	
			//Check the password
			cout << "  Password: ";
			cin >> password;
			while (!checkPass(p+format1, password))
			{
				system ("cls");
				cout << endl << endl << endl << endl << endl;
				cout << "  Wrong password, enter again or press '~' to exit: ";
				cin >> password;
				if (password == "~")
					break;
			}

			if (password == "~")
			{
				system ("cls");
				Menu ();
				break;
			}

			//Show personal data
			system ("cls");
			showData(p+format2);
			system ("pause");
			Menu ();
			break;
		}
	
		else if (select == "2")
		{
			//Show rank table
			system ("cls");
			if (!showData("rank"+format2))
			{
				for (int i = 0; i < 5; i++)
					cout << endl;
				cout << "  ";
				system ("pause");
				Menu();
			}

			else 
			{
				system ("pause");
				Menu();
			}

			break;
		}
	}
}