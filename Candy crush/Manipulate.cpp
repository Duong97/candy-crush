#include "Header.h"

void Up (char a[9][9], string select)
{
	char temp;
	temp = a[select[0]][select[1]];
	a[select[0]][select[1]] = a[select[0] -1][select[1]];
	a[select[0] -1][select[1]] = temp;

};

void Down (char a[9][9], string select)
{
	char temp;
	temp = a[select[0]][select[1]];
	a[select[0]][select[1]] = a[select[0]+1][select[1]];
	a[select[0]+1][select[1]] = temp;

};

void Left (char a[9][9], string select)
{
	char temp;
	temp = a[select[0]][select[1]];
	a[select[0]][select[1]] = a[select[0]][select[1]-1];
	a[select[0]][select[1]-1] = temp;

};

void Right (char a[9][9], string select)
{
	char temp;
	temp = a[select[0]][select[1]];
	a[select[0]][select[1]] = a[select[0]][select[1]+1];
	a[select[0]][select[1]+1] = temp;

};

void Return (char a[9][9], string k, string select)
{
	if (k == "u")
		Up(a, select);
		
	else if (k == "d")
		Down(a, select);
		
	else if (k == "l")
		Left(a, select);
		
	else if (k == "r")
		Right(a, select);
}

void Select (char a[9][9], string k, string select, char &newR, char &newC)
{
	if (k == "u")
		{
			Up(a, select);
			newR = select[0] -1;
			newC = select[1];
		}
		else if (k == "d")
		{
			Down(a, select);
			newR = select[0] +1;
			newC = select[1];
		}
		else if (k == "l")
		{
			Left(a, select);
			newR = select[0];
			newC = select[1]-1;
		}
		else if (k == "r")
		{
			Right(a, select);
			newR = select[0];
			newC = select[1]+1;
		}
}
