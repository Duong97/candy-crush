#include "Header.h"

void Random (char a[9][9], char i, char j)
{
	a[i][j] = rand () % ('5'- '1'+ 1) +'1';
};

void Initial_table (char a[9][9])
{
	for (char i = '0'; i < '9'; i++)
		for (char j = '0'; j < '9'; j++)
			Random(a, i, j);
}; 

void Show_table (char a[9][9])
{
	int check_c = 0, check_r = 0;
	for (char i = 48; i < 57; i++)
	{
		for (char j = 48; j < 57; j++)
		{
			if (check_c == 0)
			{
				TextColor (8);
				cout << "  " << setw (29) << " ";
				for (int i = 0; i < 9; i++)
					if (i == 0)
						cout << i;
					else 
						cout << setw(7) << i;

				cout << endl;

				TextColor (8);
				cout << "  " << setw (29) << " ";
				for (int i = 0; i < 9*7 - 6; i++)
					cout << "-";

				cout << endl;

				check_c = 1;
			}
			
			if (check_r == 0)
			{
				TextColor (8);
				cout << setw (29) << " " << i << "|";
				cout << Print(a[i][j]);
				check_r = 1;
			}
			else 
				cout << setw (7) << Print(a[i][j]);
		}
		check_r = 0;
		TextColor (8);
		cout << "|" << i << endl;
	}

	TextColor (8);

	cout << "  " << setw (29) << " ";
	for (int i = 0; i < 9*7 - 6; i++)
		cout << "-";
	cout << endl;
	
	cout << "  " << setw(29) << " ";
	for (int i = 0; i < 9; i++)
		if (i == 0)
			cout << i;
		else 
			cout << setw(7) << i;
	cout << endl;	
};
