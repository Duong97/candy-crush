#include <iostream>
#include <iomanip>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <time.h>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <thread>
#include "graphics.h"
#pragma comment(lib, "graphics.lib")
#pragma warning(disable : 4996)
using namespace std;

struct direction
{
	char up1, up2;
	char down1, down2;
	char right1, right2;
	char left1, left2;
	char C_r, C_l;
};

struct detail
{
	int score;
	int day;
	int month;
	int year;
	int hour;
	int min;
	int second;
};

struct player
{
	string name;
	string password;
	int Max;
	vector <detail> data;
};

struct Rank
{
	string name;
	detail data;
};

//Data
bool checkData (string input);
bool checkPass (string p, string password);
detail Max (player a);
void ReRank (vector <Rank> &a);
void updateData (string p, string password, int score);
void updateRank (string p, detail sample);

//Execute
void Reset (char a[9][9]);
void Find_first (char a[9][9], char newR, char newC, string select, string k, int &score);
void Find_after (char a[9][9], char newR, char newC, int &check, int &score);
void Scan (char a[9][9], int &score);

//file
void clearFile (string fileName);
void readFile (player &a, string input);
void writeFileNumber (string fileName, int fValue);
void writeFileString (string fileName, string strValue);
void writeFileNewLine (string fileName);
void writeFile (player a, string p, string format1, string format2);
void readRank (vector <Rank> &a, string input);
void writeRank (vector <Rank> a, string format1, string format2);
bool showData (string input);
void writeFile_process (player a, string p, string format1);
void writeFile_data (player a, string p, string format2);
void writeRank_process (vector <Rank> a, string format1);
void writeRank_data (vector <Rank> a, string format2);


//Graphic
void setFontSize (int FontSize);
void TextColor (int x);
char Print (char a);
void Implement ();
void stable_letter ();
void dynamic_letter ();
void Welcome ();

//Manipulate
void Up (char a[9][9], string select);
void Down (char a[9][9], string select);
void Left (char a[9][9], string select);
void Right (char a[9][9], string select);
void Return (char a[9][9], string k, string select);
void Select (char a[9][9], string k, string select, char &newR, char &newC);

//Match
bool match_3 (char a[9][9], direction d, int &score);
bool match_4 (char a[9][9], direction d, int &score);
bool match_5 (char a[9][9], direction d, int &score);
bool match_L (char a[9][9], direction d, int &score);
bool match_T (char a[9][9], direction d, int &score);
bool combo (char a[9][9], direction d, string select, int &score);

//Menu
void Menu ();
void execute ();
void Info ();
void User ();

//Prepare
void Random (char a[9][9], char i, char j);
void Initial_table (char a[9][9]);
void Show_table (char a[9][9]);

