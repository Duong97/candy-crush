#include "Header.h"

void Reset (char a[9][9])
{
	char start, end;
	char temp;
	for (char j = '0'; j < '9'; j++)
	{
		start = '0';
		for (char i = '0'; i < '9'; i++)
		{
			if (a[i][j] == ' ')
			{
				end = i;
				while (end != start)
				{
					temp = a[end][j];
					a[end][j] = a[end - 1][j];
					a[end - 1][j] = temp;
					end--;
				}
				start++;
			}
		}
	}

	for (char i = '0'; i < '9'; i++)
	{
		for (char j = '0'; j < '9'; j++)
			if (a[i][j] == ' ')
				Random (a, i, j);
	}
};

void Find_first (char a[9][9], char newR, char newC, string select, string k, int &score)
{
	direction d;
	d.up1 = newR - 1; d.up2 = newR - 2;
	d.down1 = newR + 1; d.down2 = newR + 2;
	d.left1 = newC - 1; d.left2 = newC - 2;
	d.right1 = newC + 1; d.right2 = newC + 2;
	d.C_r = newR; d.C_l = newC;
	bool a0, a1, a2, a3, a4, a5;

	a0 = combo (a, d, select, score);
	a1 = match_T (a, d, score);
	a2 = match_L (a, d, score);
	a3 = match_5 (a, d, score);
	a4 = match_4 (a, d, score);
	a5 = match_3(a, d, score);

	Reset (a);

	//if NO number is eliminated, back to the previous position
	if (!a0 && !a1 && !a2 && !a3 && !a4 && !a5)
		Return (a, k, select);
};

void Find_after (char a[9][9], char newR, char newC, int &check, int &score)
{
	direction d;
	d.up1 = newR - 1; d.up2 = newR - 2;
	d.down1 = newR + 1; d.down2 = newR + 2;
	d.left1 = newC - 1; d.left2 = newC - 2;
	d.right1 = newC + 1; d.right2 = newC + 2;
	d.C_r = newR; d.C_l = newC;
	bool a0, a1, a2, a3, a4;

	a0 = match_T (a, d, score);
	a1 = match_L (a, d, score);
	a2 = match_5 (a, d, score);
	a3 = match_4 (a, d, score);
	a4 = match_3(a, d, score);

	Reset (a);

	if (a0 || a1 || a2 || a3 || a4)
		check = 1;
};

void Scan (char a[9][9], int &score)
{
	int check;
	while (true)
	{
		check = 0;
		for (char i = '0'; i < '9'; i++)
			for (char j = '0'; j < '9'; j++)
				Find_after (a, i, j, check, score);
		if (check == 0)
			break;
	}
};